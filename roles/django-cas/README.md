# Django CAS

Une fois le rôle appliqué il faut aller dans django_cas.path et faire un `./manage.py collectstatic`.

## Variables

On rassemble dans le dictionnaire django_cas toutes les variables liées au déploiement du cas. Voici une liste exhaustive des paramètres à définir :

  - repo : Endroit d'où cloner les sources. Par défaut au crans, on utilise le dépôt : https://gitlab.crans.org/nounous/django-cas.git
  - path : Là où on va installer le logiciel.
  - url : Une liste d'url qui vont servir le cas
  - ldap : dictionnaire qui configure la discussion avec le ldap
    - dn : nom derrière lequel sont stockés les utilisateurs dans le ldap
    - password : mot de passe de connection au ldap
    - user : utilisateur avec lequel on se connecte
    - server : serveur sur lequel le ldap est situé
  - db : dictionnaire qui configure la discussion avec la base de données
    - host : l'hôte sur lequel la base de données se trouve
    - password : mot de passe d'accès
  - secret_key : le secret de django_cas
  - reverse_proxy : liste optionnelle permettant de faire tourner le cas derrière un reverse proxy tout en gardant les informations de connection originales de l'utilisateur. Il doit contenir la liste d'ip du reverse-proxy avec lequel il se connectera au CAS.
