# Framadate

Ce rôle installe un serveur framadate.

## Variables

  - glob_framadate:
    - contact: Adresse mail de contact de l'administrateur
    - automatic_response: Adresse mail utilisée pour répondre au clients du
      serveur
    - smtp_server: le serveur smtp a utilisé pour envoyer les mails
    - hostname: le nom de domaine du serveur
    - repo: le repo git où est stocké le code source de framadate
    - versoin: le commit dans lequel on se place
    - admin_username: le nom d'utilisateur de l'administrateur
    - admin_password: le mot de l'administrateur
    - db_password: le mot de passe de la base de données
  - loc_framadate:
    - path: le chemin où on installe framadate

