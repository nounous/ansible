# KEEPALIVED

Ce rôle installe keepalived pour permettre la redondance de certain service
entre plusieurs services.
/!\ Ce rôle déploie un script pour relancer automatiquement le serveur dhcp /!\

## VARS

keepalived:
  - mail_destination: a qui envoyé les mails en cas de switching
  - mail_source: qui envoie les mails
  - smtp_server: le serveur smtp par qui passer pour envoyer les mails
  - pool: Une liste de différentes instances installable sur la machine. Les
    instances sont des dictionnaires comprenant les champs suivant :
    - name: le nom de l'instance
    - password: le mot de passe que vont utilisé les marchines d'une même
      instance pour se synchroniser
    - id: l'indentifiant qu'elles vont utiliser pour discuter
    - ipv6: s'il est necessaire de configurer une instance supplémentaire pour
      de l'ipv6
    - notify: le script a notifé en cas de switching (s'il n'est pas précisé
      aucun script n'est utilisé)
    - administration: le vlan d'administration sur lequel les machines d'une
      même instances vont discuter
    - zones: une liste de zone sur lequel vont parler les instances keepalived.
      Chaque zone est un disctionnaire comprenant les champs suivants:
      - vlan: le vlan sur lequel est installé la zone
      - ipv4: l'ipv4 au format CIDR partagé par les machines
      - brd: s'il faut préciser ou non l'interface de broadcast
      - ipv6: une ipv6 (elle peut ne pas être précisé, si elle est présente mais
        que l'instance ne précise pas ipv6, elle sera ignoré)
  - instances: Une liste d'instance a déployer sur la machine. Les instances
    sont des dictionnaires comprenant les champs suivants:
    - name: le nom de linstance a deployer
    - tag: le petit nom à lui donner
    - state: l'état (entre BACKUP et MASTER)
    - priority: la priorité (pour un MASTER on met par défaut 150 puis on reduit
      de 50 par 50)
