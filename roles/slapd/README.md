# SLAPD

Deploie un serveur ldap master ou replica

## VARS

slapd:
  - ip : l'ip sur lequel il va installer le serveur ldap
  - replica : s'il s'agit d'un master ou d'une replica
  - replica_rid : le numéro de replica du serveur
  - master_ip : l'ip du master
  - replication_credentials : les credientials pour authentifier les replicas
    auprès du master
