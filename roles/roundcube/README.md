# ROUNDCUBE

Ce rôle installe roundcube sur un serveur.

## VARS

roundcube :
  - name: le nom sans espace du serveur
  - imap_server: l'uri du serveur imap
  - smtp_server: l'uri du serveur smtp
  - des_key: une clé pour chiffrer les mots de passes des utilisateurs. Elle
    doit faire exactement 24 caractères
  - mail_domain: le nom de domaine des mails
  - pgsql_server: l'uri du serveur sur lequel se trouve la base de données au
    format postgresql.
  - plugins: une liste de plugin a chargé. Chaque élément est une liste
    comprenant:
    - repo: le repo git où est stocké le plugin
    - name: le nom du plugin
    - version: la version du plugin
