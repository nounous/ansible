---
- name: Install some front APT packages
  apt:
    install_recommends: false
    update_cache: true
    name:
      - python3-django-crispy-forms
      - python3-django-filters
      - python3-djangorestframework
      - python3-django-tables2
      - python3-docutils
  register: apt_result
  retries: 3
  until: apt_result is succeeded

- name: Install some front pip packages
  pip:
    name:
      - git+https://gitlab.adm.crans.org/nounous/crispy-bootstrap5.git

- name: Set data directories in development mode
  when: constellation.version != "master"
  set_fact:
    project_path: /var/local/constellation
    module_path: /var/local/constellation/constellation

- name: Set data directories in production mode
  when: constellation.version == "master"
  set_fact:
    project_path: /usr/local/lib/python3.9/dist-packages/constellation
    module_path: /usr/local/lib/python3.9/dist-packages/constellation

- name: Check front dependencies (production)
  when: constellation.version == "master"
  pip:
    name:
      - git+https://gitlab.adm.crans.org/nounous/constellation.git[front]
    state: latest

- name: Install uWSGI
  apt:
    install_recommends: false
    update_cache: true
    name:
      - uwsgi
      - uwsgi-plugin-python3
  register: apt_result
  retries: 3
  until: apt_result is succeeded

- name: Copy constellation uWSGI app
  template:
    src: uwsgi/apps-available/constellation.ini.j2
    dest: /etc/uwsgi/apps-available/constellation.ini
    owner: root
    group: root
    mode: 0644
  notify: Restart uWSGI

- name: Activate constellation uWSGI app
  file:
    src: ../apps-available/constellation.ini
    dest: /etc/uwsgi/apps-enabled/constellation.ini
    owner: root
    group: root
    state: link
  ignore_errors: "{{ ansible_check_mode }}"
  notify: Restart uWSGI

# In the future, migrations will be included in the repository.
- name: Make Django migrations
  django_manage:
    command: makemigrations
    project_path: "{{ project_path }}"
  notify: Restart uWSGI

- name: Migrate database
  django_manage:
    command: migrate
    project_path: "{{ project_path }}"
  notify: Restart uWSGI

- name: Create static files directory
  file:
    path: /var/lib/constellation/{{ item }}
    state: directory
    mode: "2775"
    owner: www-data
    group: "{{ constellation.group }}"
    recurse: true
  loop:
    - static
    - media

- name: Symlink static and media directories (dev)
  file:
    src: /var/lib/constellation/{{ item }}
    dest: /var/local/constellation/{{ item }}
    state: link
    owner: www-data
    group: "{{ constellation.group }}"
  loop:
    - static
    - media

- name: Collect static files
  django_manage:
    command: collectstatic
    project_path: "{{ project_path }}"
  notify: Restart uWSGI
