#!/bin/bash
obsolete="$(comm -23 \
  <(dpkg-query -W -f '${db:Status-Abbrev}\t${Package}\n' \
    | grep '^.[^nc]' | cut -f2 | sort) \
  <(apt-cache dumpavail | sed -rn 's/^Package: (.*)/\1/p' | sort -u) \
  | awk 'END{printf "apt_obsolete %d", NR}'
)"

echo "# HELP apt_obsolete Apt obsolete package."
echo "# TYPE apt_obsolete gauge"
echo "$obsolete"
