# Recettes Ansible du Cr@ns

Ensemble des recettes de déploiement Ansible pour les serveurs du Crans.
Pour les utiliser, vérifiez que vous avez au moins Ansible 2.10.

## Ansible 101

Si vous n'avez jamais touché à Ansible avant, voilà une rapide introduction.

**Inventory** : c'est le fichier `hosts` d'inventaire.
Il contient la définition de chaque machine et le regroupement.

Quand on regroupe avec un `:children` en réalité on groupe des groupes.

Chaque machine est annoncée avec son hostname. Il faut pouvoir SSH sur cette machine
avec ce hostname, car c'est ce qu'Ansible fera.

**Playbook** : c'est une politique de déploiement.
Il contient les associations des rôles avec les machines.

L'idée au Crans est de regrouper par thème. Exemple, le playbook `monitoring.yml`
va contenir toutes les définitions machines-rôles qui touchent au monitoring.
Cela permet de déployer manuellement tout le monitoring sans toucher au reste.

**Rôle** : un playbook donne des rôles à des machines. Ces rôles sont tous dans
le dossier `roles/`. Un rôle installe un service précis sur un serveur.

Il est préférable d'être atomique sur les rôles plutôt d'en coder un énorme
qui sera difficilement maintenable.

*Exemples de rôle* : activer les backports pour ma version de Debian, installer NodeJS,
déployer un serveur prometheus, déployer une node prometheus…

**Tâche** : un rôle est composé de tâches. Une tâche effectue une et une seule
action. Elle est associée à un module Ansible.

*Exemples de tâche* : installer un paquet avec le module `apt`, ajouter une ligne dans
un fichier avec le module `lineinfile`, copier une template avec le module `template`…

Une tâche peut avoir des paramètres supplémentaires pour la réessayer quand elle plante,
récupérer son résultat dans une varible, mettre une boucle dessus, mettre des conditions…

N'oubliez pas d'aller lire l'excellent documentation de RedHat sur tous les modules
d'Ansible !

### Gestion des groupes de machines

Pour la majorité des groupes on retrouve ce qu'il y avait dans BCFG2
(`crans_vm`, `crans`…).
Pour la liste complète, je vous invite à lire le fichier `hosts`.

Néanmoins grâce au système de facts d'Ansible, les groupes suivants ont été
remplacés par une condition :

  * pour tester les versions de Debian,

    ```YAML
    ansible_lsb.codename == 'bullseye'
    ```

  * pour tester si c'est un CPU Intel x86_64,

    ```YAML
    ansible_processor[0].find('Intel') != -1
    and ansible_architecture == 'x86_64'
    ```

### Lister tout ce que sait Ansible sur un hôte

Lors du lancement d'Ansible, il collecte un ensemble de faits sur les serveurs
qui peuvent ensuite être utilisés dans des variables.
Pour lister tous les faits qu'Ansible collecte nativement d'un serveur
on peut exécuter le module `setup` manuellement.

```
ansible zamok.adm.crans.org -m setup
```

## Exécution d'Ansible

### Configurer la connexion au vlan adm

Envoyer son agent SSH peut être dangereux
([source](https://heipei.io/2015/02/26/SSH-Agent-Forwarding-considered-harmful/)).

On va utiliser plutôt `ProxyJump`.
Dans la configuration SSH :

```
# Use a proxy jump server to log on all Crans inventory
Host 172.16.10.* *.adm.crans.org
    User _user
    ProxyJump en7.crans.org
```

Il faut sa clé SSH configurée sur le serveur que l'on déploit.
```bash
ssh-copy-id zamok.adm.crans.org
```

### Connexion au LDAP

De nombreuses informations sont stockées dans le LDAP, situé sur tealc. Les
différents rôles Ansible vont parfois récupérer des données. Vous devez pour
cela rendre accessible le LDAP sur votre machine, en ouvrant dans un terminal à
côté :

```bash
ssh -L 1636:tealc.adm.crans.org:636 tealc.adm.crans.org
```

Pour que la connexion puisse fonctionner, vous aurez besoin du paquet
`python3-ldap`.


### Lancer un Playbook Ansible

Pour tester le playbook `root.yml` :
```bash
plays/root.yml -l voyager.adm.crans.org --check
```

Vous pouvez ensuite enlever `--check` si vous voulez appliquer les changements !

Vous pouvez aussi avoir un mode étape par étape avec `--step`.
